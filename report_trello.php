<?
set_time_limit(360000);
require "TrelloApi.class.php";


$trello = new TrelloApi( '[[USER_KEY]]', '[[API_KEY]]', 'REQUEST_DEBUG_DUMP_FILE.txt' );


$arr_boards = json_decode($trello->get( 'members/me/boards' ));

$html = '<table>';

$html .= '<tr>';
$html .= '<td>Board</td>';
$html .= '<td>List</td>';
$html .= '<td>Card</td>';
$html .= '<td>Membro</td>';
$html .= '<td>Labels</td>';
$html .= '<td>Entrega</td>';
$html .= '<td>Checklist</td>';
$html .= '<td>Link</td>';

$html .= '</tr>';

foreach($arr_boards as $key => $board){

	

		$arr_cards = json_decode($trello->get( 'boards/'.$board->id.'/cards' ));
		if(!empty($arr_cards)){
			foreach ($arr_cards as $key => $card) {
				$arr_members = $card->idMembers;
				//Se tem membros ou não
				if(!empty($arr_members)){

					foreach ($arr_members as $key => $member) {

						$html .= '<tr>';

						$html .= '<td>'.$board->name.'</td>';
						$html .= '<td>'.getNameList($trello,$card->idList).'</td>';
						$html .= '<td>'.$card->name.'</td>';
						$html .= '<td>'.getNameMember($trello,$member).'</td>';
						$html .= '<td>'.getNameLabels($card->labels).'</td>';
						$html .= '<td>'.substr($card->due,0,10).'</td>';
						$html .= '<td>'.getChecklist($trello,$card->idChecklists).'</td>';
						$html .= '<td>'.$card->url.'</td>';					
						$html .= '</tr>';			
						
						// echo $html;
						// die();
					}	


				}


			}
		}

	
 }

$html .= '</table>';

echo $html;

function getNameMember($trello_obj,$id_member){

	$arr_member = json_decode($trello_obj->get('members/'.$id_member));
	$name = '';
	
	if(!empty($arr_member)){
		$name = $arr_member->fullName;
	}

	return $name;
}

function getNameList($trello_obj,$id_list){

	$arr_list = json_decode($trello_obj->get('list/'.$id_list));
	$name = '';
	if(!empty($arr_list)){
		$name = $arr_list->name;
	}

	return $name;

}

function getNameLabels($arr_id_labels){
	$labels = array();

	if(!empty($arr_id_labels)){
		foreach($arr_id_labels as $key){
			array_push($labels, $key->name);
		}
	}

	return implode($labels,", ");
}



function getChecklist($trello_obj,$id_checklist){

	$result = array();

	if(!empty($id_checklist)){

		foreach($id_checklist as $k){

			$arr_check = json_decode($trello_obj->get('/checklists/'.$k));

			

			if(!empty($arr_check)){
				$complete = 1;
				$total = 1;
				foreach ($arr_check as $key => $value) {

					if($value->state == "complete"){
						$complete++;
					}

					$total = count($key->checkItems);
				}

				$check_list = $complete." / ".$total;
			}


			array_push($result, $check_list);
			$check_list = '';
			$complete = 1;
			$total = 1;
			unset($arr_check);

		}


	}

	return implode($result," | ");
}

?>
