<?php
/**
 * Basic Trello API wrapper. Supports GET, POST, PUT and DELETE HTTP methods.
 * All GET, POST, PUT and DELETE path's and params are here: https://trello.com/docs/api/index.html.
 * 
 * @author André Dal Molin <admolindasilva@gmail.com>
 * @version 1.0
 * @since 2014-03-20
 */
class TrelloApi {
    /**
     * @var string Trello API host.
     */
    private $apiHost = 'api.trello.com';
    
    /**
     * @var integer Trello API version.
     */
    private $apiVersion = 1;
    
    /**
     * @var string Trello API user key.
     */
    private $apiKey;
    
    /**
     * @var string Trello API user token.
     */
    private $apiToken;
    
    /**
     * @var bool Makes or not cURL to output the raw request to an text file.
     */
    private $cURLDebug;

    private $numberCalls = 0;
    
    /**
     * Construct function of the class. Only set some variables.
     * 
     * @param string $apiKey The API Key. See: https://trello.com/docs/gettingstarted/index.html#getting-an-application-key.
     * @param string $apiToken The API Token. See: https://trello.com/docs/gettingstarted/index.html#getting-a-token-from-a-user.
     * @param bool|string $cURLDebug If $debug is a string, cURL will save the whole request in a file named $debug.
     * @return \TrelloApi
     */
    public function __construct( $apiKey, $apiToken, $cURLDebug = false ) {
        $this->apiKey = $apiKey;
        $this->apiToken = $apiToken;
        $this->cURLDebug = $cURLDebug;

        return $this;
    }
    
    /**
     * Performs an GET HTTP request to Trello API.
     * 
     * @param string $path The path of the request. See trello.com/docs/api.
     * @param array $parameters The parameters of the request.
     * @return string The request response.
     */
    public function get( $path, array $parameters = array() ) {
        return $this->request( $path, $parameters );
    }

    /**
     * Performs an POST HTTP request to Trello API.
     * 
     * @param string $path The path of the request. See trello.com/docs/api.
     * @param array $parameters The parameters of the request.
     * @return string The request response.
     */
    public function post( $path, array $parameters = array() ) {
        return $this->request( $path, $parameters, 'POST' );
    }
    
    /**
     * Performs an PUT HTTP request to Trello API.
     * 
     * @param string $path The path of the request. See trello.com/docs/api.
     * @param array $parameters The parameters of the request.
     * @return string The request response.
     */
    public function put( $path, array $parameters = array() ) {
        return $this->request( $path, $parameters, 'PUT' );
    }
    
    /**
     * Performs an DELETE HTTP request to Trello API.
     * 
     * @param string $path The path of the request. See trello.com/docs/api.
     * @param array $parameters The parameters of the request.
     * @return string The request response.
     */
    public function delete( $path, array $parameters = array() ) {
        return $this->request( $path, $parameters, 'DELETE' );
    }
    
    /**
     * This internal method generates the request URL used by cURL to call Trello API.
     * 
     * @param string $path The path of the request. See trello.com/docs/api.
     * @param array $parameters The request parameters ('key' => 'value').
     * @param string $method The HTTP method of the request (GET, POST, PUT, DELETE).
     * @return string The formed URL that cURL uses.
     */
    private function buildRequestUrl( $path, $parameters, $method ) {
        switch( $method ) {
            case 'GET':
                return sprintf( 'https://%s/%d/%s?%s', $this->apiHost, $this->apiVersion, $path, http_build_query( array_merge( array( 'key' => $this->apiKey, 'token' => $this->apiToken ), $parameters ) ) );

            case 'POST':
            case 'PUT':
            case 'DELETE':
                return sprintf( 'https://%s/%d/%s?%s', $this->apiHost, $this->apiVersion, $path, http_build_query( array( 'key' => $this->apiKey, 'token' => $this->apiToken ) ) );
        }
    }

    /**
     * This is the core of this lib. Makes the request to Trello API using cURL based on the $method
     * and the $parameters.
     * 
     * @param string $path The path of the request. See trello.com/docs/api.
     * @param array $parameters The parameters of the request.
     * @param string $method The HTTP method of the request (GET, POST, PUT, DELETE).
     * @param bool $debug Set this to true to make cURL output the raw request to debug.txt.
     * @return string The request response.
     */
    private function request( $path, $parameters, $method = 'GET' ) {

        $this->verifyLimitCalls();

        $cURL = curl_init( $this->buildRequestUrl( $path, $parameters, $method ) );
        
        switch( $method ) {
            case 'POST':
                curl_setopt( $cURL, CURLOPT_POST, true );
                curl_setopt( $cURL, CURLOPT_POSTFIELDS, $parameters );
                break;

            case 'PUT':
            case 'DELETE':
                curl_setopt( $cURL, CURLOPT_CUSTOMREQUEST, $method );
                curl_setopt( $cURL, CURLOPT_POSTFIELDS, http_build_query( $parameters ) ); // Trello only validates an PUT or DELETE request if the post fields are in "query format".
                break;
        }
        
        curl_setopt( $cURL, CURLOPT_AUTOREFERER, true );
        curl_setopt( $cURL, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt( $cURL, CURLOPT_HEADER, false );
        curl_setopt( $cURL, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $cURL, CURLOPT_SSL_VERIFYPEER, false );
        
        // If the $debug param is not false, cURL will write on $debug file the whole request for debuging.
        // If the file do not exists, it will be created.
        if ( is_string( $this->cURLDebug ) && !empty( $this->cURLDebug ) ) {
            curl_setopt( $cURL, CURLOPT_VERBOSE, true );
            curl_setopt( $cURL, CURLOPT_STDERR, fopen( (string) $this->cURLDebug, 'w+' ) );
        }
        
        $response = curl_exec( $cURL );
        curl_close( $cURL );
        
        $this->setNumberCall();

        return $response;
    }


    private function setNumberCall(){
        $numberCalls++;
    }

    private function getNumberCall(){
        return $numberCalls;
    }

    private function emptyNumberCall(){
        $numberCalls = 0;
    }

    private function verifyLimitCalls(){
        if($this->getNumberCall >= 290){
            sleep(10);
            $this->emptyNumberCall();
        }
    }
}

// /**
//  * Usage exemples.
//  * All API calls returns the raw JSON data from Trello. Use json_decode to turn into an object.
//  */
// $trello = new TrelloApi( 'YOUR_API_KEY', 'YOUR_API_TOKEN', 'REQUEST_"DEBUG"_DUMP_FILE.txt' );

// // Gets all information about the current user (the user is related to the generated API_KEY).
// // See the docs from __construct method.
// $trello->get( 'members/me' );

// // Gets all the boards inside an organization.
// $trello->get( 'organizations/{ORGANIZATION_ID}/boards' );

// // Gets all the cards inside an board.
// $trello->get( 'boards/{BOARD_ID}/cards' );

// // Gets all the lists inside an board.
// $trello->get( 'boards/{BOARD_ID}/lists' );

// // Gets all the cards inside an list.
// $trello->get( 'lists/{LIST_ID}/cards' );

// // Create an card.
// $trello->post( 'cards', array(
//     'name' => 'The name of the card',
//     'desc' => 'The description of the card.',
//     'pos' => 'top', // The position of the card inside the list. Can be 'top', 'bottom' or a positive integer.
//     'due' => date( 'c', strtotime( '2015-02-22 15:00:00' ) ), // Due date in ISO 8601 format.
//     'labels' => 'Label 1, Label 2',
//     'idList' => '{LIST_ID}', // The ID of the list that the card should be inserted into.
//     'idMembers' => '{MEMBER_ID_1},{MEMBER_ID_2}', // Card members ID separated by comma.
//     // There two last parameters are to copy from another card. See: https://trello.com/docs/api/card/index.html#post-1-cards.
//     'idCardSource' => '{CARD_ID}', // The card ID to copy from.
//     'keepFromSource' => 'name,desc' // The params to copy from the source card. In this case, name and description. Put 'all' to copy the entire card.
// ) );

// // Create a comment in a card.
// $trello->post( 'cards/{CARD_ID}/actions/comments', array(
//     'text' => 'The comment.'
// ) );

// // Attach an local file to a card.
// $trello->post( 'cards/{CARD_ID}/attachments', array(
//     'name' => 'file.example',
//     'file' => sprintf( '@%s;filename=%s', basename( 'path/to/file.example' ), 'file.example' ),
//     'mimeType' => finfo_file( finfo_open( FILEINFO_MIME_TYPE ), 'path/to/file.example' )
// ) );

// // Attach an external file to a card.
// $trello->post( 'cards/{CARD_ID}/attachments', array(
//     'name' => 'file.example',
//     'url' => 'http://example.com/path/to/file.example'
// ) );

// // Move an card to another list.
// $trello->put( 'cards/{CARD_ID}/idList', array(
//     'value' => '{LIST_ID}' // The ID of the list the card should be moved to.
// ) );

// // Delete an card.
// $trello->delete( 'cards/{CARD_ID}' );

// /* ------------------------------------------------------------------------------------------------- */
// /* ----- Wanna see all Trello API possibilities? Check: https://trello.com/docs/api/index.html ----- */
// /* ------------------------------------------------------------------------------------------------- */